package ru.t1.vlvov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void info(@Nullable String message);

    void debug(@Nullable String message);

    void command(@Nullable String message);

    void error(@NotNull Exception e);

}
